package fr.unice.iut.info.s3a.handson;


public class Calculatrice {

    public int add(int a, int b){
        return a + b;
    }

    public int sub(int a, int b){
        return a - b;
    }

    public int mult(int a, int b){
        return a * b;
    }

    public int div(int a, int b) throws ArithmeticException{
        if (b != 0)
            return a / b;
        else throw new ArithmeticException("Division by 0");
    }

    public int abs(int a){
        return (a >= 0) ? a : -a;
    }

    public int mod(int a, int b){
        return sub(a, mult(b, div (a, b)));
    }


}
