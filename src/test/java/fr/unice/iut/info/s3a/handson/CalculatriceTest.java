package fr.unice.iut.info.s3a.handson;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatriceTest {

    private Calculatrice testObject;

    @Before
    public void before(){
        this.testObject = new Calculatrice();
    }

    @Test
    public void testAdd(){
        assertEquals(5, testObject.add(3, 2));
    }

    @Test
    public void testSub() {
        assertEquals(-2, testObject.sub(3, 5));
    }

    @Test
    public void testMult(){
        assertEquals(30, testObject.mult(15,2));
    }

    @Test
    public void testDiv(){
        assertEquals(2, testObject.div(10,5));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivBy0() throws Exception{
        testObject.div(2, 0);
    }

    @Test
    public void testAbs() {
        assertEquals(3,testObject.abs(3));
    }

    @Test
    public void testAbs2()  {
        assertEquals(3, testObject.abs(-3));
    }

    @Test
    public void testMod() {
        assertEquals(2, testObject.mod(7, 5));
    }

    @Test(expected = ArithmeticException.class)
    public void testModWith0() {
        testObject.mod(7, 0);
    }
}